<div class="flex items-baseline space-x-4 ">
    <h2>{{ __('Config') }}</h2>
    <div class="text-gray-300">|</div>
    <a href="{{ route('config.hosts') }}">
        <div class="@if(request()->is('config/hosts'))border-b-2 @endif">
            {{ __('Hosts') }}
        </div>
    </a>

    <div class="text-gray-300">|</div>
    <a href="{{ route('config.syncs') }}">
        <div class="@if(request()->is('config/syncs'))border-b-2 @endif">
            {{ __('Syncs') }}
        </div>
    </a>
</div>

<div class="w-full flex justify-end">
    <div class="w-32 text-right">
        <x-form-select name="linesPerPage" :options="$linesOptions" label="Lines per page" />
    </div>
</div>


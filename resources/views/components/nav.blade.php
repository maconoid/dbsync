<nav class="bg-gray-800">
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex items-center justify-between h-16">
            <div class="flex items-center">
                <div class="flex-shrink-0">
                    <img  src="{{ asset('image/logo.svg') }}" alt="dbsync" />
                </div>
                <div class="hidden md:block">
                    <div class="ml-10 flex items-baseline space-x-4">
                        <a href="{{ route('home') }}"
                           class="nav__link @if(request()->routeIs('home')) nav__link--active @endif ">Dashboard</a>

                        <a href="{{ route('logs') }}"
                           class="nav__link @if(request()->routeIs('logs')) nav__link--active @endif">Logs</a>

                        <a href="{{ route('config') }}"
                           class="nav__link @if(request()->is('config*')) nav__link--active @endif">Config</a>


                    </div>
                </div>
            </div>

        </div>


    </div>

</nav>

<div wire:poll.visible.10s>
    @wire
    @include('components.linesPerPage')
    @endwire
    <table class="dataGrid">
        <thead>
        <tr>

            <th wire:click="sortBy('sync_id')">{{ __('Sync name') }}</th>
            <th wire:click="sortBy('action_id')">{{ __('Action') }}</th>
            <th wire:click="sortBy('status')">{{ __('Status') }}</th>
            <th wire:click="sortBy('created_at')"> {{ __('Created at') }} </th>
            <th wire:click="sortBy('created_at')"> {{ __('Updated at') }} </th>
            <th></th>
        </tr>
        </thead>
        <tbody>


        @foreach ($logs as $index => $log)
            <tr>

                <td>{{ $log->sync->syncname }} </td>
                <td>{{ $log->action->name }} </td>
                <td>{{ $log->status }} </td>
                <td>
                    <div class="text-sm leading-5 text-gray-900">
                        {{ $log->created_at->diffForHumans() }}
                    </div>
                    <div class="text-sm leading-5 text-gray-500">
                        {{ $log->created_at->format('d-m-Y H:i') }}
                    </div>
                </td>
                <td>
                    <div class="text-sm leading-5 text-gray-900">
                        {{ $log->updated_at->diffForHumans() }}
                    </div>
                    <div class="text-sm leading-5 text-gray-500">
                        {{ $log->updated_at->format('d-m-Y H:i') }}
                    </div>
                </td>

            </tr>
            @if($log->errormessage)
            <tr>
                <td class="dataGrid__code" colspan="5">{{ $log->errormessage }} </td>
            </tr>
            @endif
        @endforeach
        </tbody>
    </table>
    {{ $logs->links() }}

</div>

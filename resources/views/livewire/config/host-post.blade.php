<form wire:submit.prevent="save">
    <div>
        @wire

        <x-form-input name="hostname" label="Hostname" />
        <x-form-group name="can_ssh" label="Can SSH" inline>
            <x-form-radio name="can_ssh" value="True" label="True" />
            <x-form-radio name="can_ssh" value="False" label="False" />
        </x-form-group>
        @endwire
        <x-form-submit>Add host</x-form-submit>

    </div>

</form>

<x-slot name="header">
    @include('components.navConfig')
</x-slot>
<div>
    <table class="dataGrid">
        <thead>
        <tr>
            <th class="" wire:click="sortBy('hostname')">
                {{ __('Hostname') }}
            </th>
            <th class="" wire:click="sortBy('can_ssh')">
                {{ __('Can SSL') }}
            </th>
            <th class="" wire:click="sortBy('created_at')">
                {{ __('Created at') }}
            </th>
            <th class="" wire:click="sortBy('updated_at')">
                {{ __('Updated at') }}
            </th>
            <th class=""></th>
        </tr>
        </thead>
        <tbody class="bg-white divide-y divide-gray-200">
            @foreach ($hosts as $index => $host)
                <tr>
                    <td class="">
                        {{ $host->hostname }}
                    </td>
                    <td class="">
                        {{ $host->can_ssh }}
                    </td>
                    <td>
                        <div class="text-sm leading-5 text-gray-900">
                            {{ $host->created_at->diffForHumans() }}
                        </div>
                        <div class="text-sm leading-5 text-gray-500">
                            {{ $host->created_at->format('d-m-Y H:i') }}
                        </div>
                    </td>
                    <td>
                        <div class="text-sm leading-5 text-gray-900">
                            {{ $host->updated_at->diffForHumans() }}
                        </div>
                        <div class="text-sm leading-5 text-gray-500">
                            {{ $host->updated_at->format('d-m-Y H:i') }}
                        </div>
                    </td>
                    <td class=" text-right text-sm font-medium">
                        <button wire:click="delete({{ $host->id }})">Delete</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $hosts->links() }}

    @livewire('config.host-post')


</div>

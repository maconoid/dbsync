<div>
    <form wire:submit.prevent="save">
        @wire
        <x-form-input name="syncname" label="Sync Name" />
        <x-form-select name="srchost" label="Source host" :options="$hosts"  />
        <x-form-input name="srcdir" label="Source Directory" />

        <x-form-select name="dsthost" label="Destination host" :options="$hosts"  />
        <x-form-input name="dstdir" label="Destination Directory" />

        <x-form-select name="dbtype" :options="['mysql']" />




        @endwire
        <x-form-submit>Add sync</x-form-submit>

    </form>
</div>

<x-slot name="header">
    @include('components.navConfig')
</x-slot>
<div>
    <table class="dataGrid">
        <thead>
        <tr>
            <th wire:click="sortBy('id')"> {{ __('#') }}</th>
            <th wire:click="sortBy('syncname')"> {{ __('Name') }}</th>
            <th wire:click="sortBy('dbtype')"> {{ __('Type') }}</th>
            <th wire:click="sortBy('srchost')"> {{ __('Source host') }}</th>
            <th wire:click="sortBy('srcdir')"> {{ __('Source dir') }}</th>
            <th wire:click="sortBy('dsthost')"> {{ __('Dest host') }}</th>
            <th wire:click="sortBy('dstdir')"> {{ __('Dest dir') }}</th>
            <th wire:click="sortBy('created_at')"> {{ __('Created at') }} </th>
            <th wire:click="sortBy('updated_at')"> {{ __('Updated at') }} </th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($syncs as $index => $sync)
            <tr>
                <td>{{ $sync->id }} </td>
                <td>{{ $sync->syncname }} </td>
                <td>{{ $sync->dbtype }} </td>
                <td>{{ $sync->sourceHost->hostname }} </td>
                <td>{{ $sync->srcdir }} </td>
                <td>{{ $sync->destinationHost->hostname }} </td>
                <td>{{ $sync->dstdir }} </td>
                <td>
                    <div class="text-sm leading-5 text-gray-900">
                        {{ $sync->created_at->diffForHumans() }}
                    </div>
                    <div class="text-sm leading-5 text-gray-500">
                        {{ $sync->created_at->format('d-m-Y H:i') }}
                    </div>
                </td>
                <td>
                    <div class="text-sm leading-5 text-gray-900">
                        {{ $sync->updated_at->diffForHumans() }}
                    </div>
                    <div class="text-sm leading-5 text-gray-500">
                        {{ $sync->updated_at->format('d-m-Y H:i') }}
                    </div>
                </td>
                <td>
                    <button wire:click="delete({{ $sync->id }})">Delete</button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $syncs->links() }}

    @livewire('config.sync-post')
</div>

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', \App\Http\Livewire\Dashboard::class)->name('home');

Route::get('config', function () {
    return view('config');
})->name('config');

Route::get('config/hosts', \App\Http\Livewire\Config\Hosts::class)->name('config.hosts');
Route::get('config/syncs', \App\Http\Livewire\Config\Syncs::class)->name('config.syncs');

Route::get('logs', \App\Http\Livewire\Log::class)->name('logs');


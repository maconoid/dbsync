<?php

namespace App\Http\Traits;

trait WithInlineValidation
{
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
}

<?php

namespace App\Http\Traits;

trait WithSorting
{
    public $sortBy        = '';
    public $sortDirection = 'asc';
    public $linesPerPage  = 10;
    public $linesOptions =
        [
            5   => 5,
            10  => 10,
            25  => 25,
            50  => 50,
            100 => 100,
        ];

    public function sortBy($field)
    {
        $this->sortDirection = $this->sortBy === $field
            ? $this->reverseSort()
            : 'asc';

        $this->sortBy = $field;
    }

    public function reverseSort()
    {
        return $this->sortDirection === 'asc'
            ? 'desc'
            : 'asc';
    }

    public function mountWithSorting()
    {
        //
    }

    public function updatingWithSorting($name, $value)
    {
        //
    }

    public function updatedWithSorting($name, $value)
    {
        //
    }

    public function hydrateWithSorting()
    {
        //
    }

    public function dehydrateWithSorting()
    {
        //
    }

    public function renderingWithSorting()
    {
        //
    }

    public function renderedWithSorting($view)
    {
        //
    }
}


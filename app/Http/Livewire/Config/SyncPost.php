<?php

namespace App\Http\Livewire\Config;

use App\Http\Traits\WithInlineValidation;
use App\Models\Host;
use App\Models\Sync;
use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class SyncPost extends Component
{
    use WithInlineValidation;

    public $syncname;
    public $srchost;
    public $srcdir;
    public $dsthost;
    public $dstdir;
    public $dbtype = 'mysql';
    public $hosts;
    public $rules = [
        'syncname' => 'required|string|min:4',
        'srchost'  => 'required|int',
        'srcdir'   => 'required|string|min:4',
        'dsthost'  => 'required|int',
        'dstdir'   => 'required|string|min:4',
        'dbtype'   => 'required|string',
    ];

    public function mount()
    {
        $hosts = Host::all(['id', 'hostname']);

        $this->hosts = [0 => __('Choose host')];
        /** @var Host $host */
        foreach ($hosts as $host) {
            $this->hosts[$host->id] = $host->hostname;
        }
    }

    public function render()
    {
        return view('livewire.config.sync-post');
    }

    public function save()
    {
        $this->validate();

        $sync = new Sync();
        $sync->fill(
            [

                'syncname' => $this->syncname,
                'srchost'  => $this->srchost,
                'srcdir'   => $this->srcdir,
                'dsthost'  => $this->dsthost,
                'dstdir'   => $this->dstdir,
                'dbtype'   => $this->dbtype,
            ]
        );

        $sync->save();

        $this->emit('refreshParent');
        $this->cleanVars();
    }

    protected function cleanVars()
    {
        $this->syncname = null;
        $this->srchost  = null;
        $this->srcdir   = null;
        $this->dsthost  = null;
        $this->dstdir   = null;
        $this->dbtype   = 'mysql';
    }
}

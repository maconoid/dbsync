<?php

namespace App\Http\Livewire\Config;

use App\Http\Traits\WithSorting;
use App\Models\Host;

use Livewire\Component;
use Livewire\WithPagination;

class Hosts extends Component
{
    use WithPagination;
    use WithSorting;


    protected $listeners = [
        'refreshParent' => '$refresh',
    ];

    public function mount()
    {
        $this->sortBy = 'hostname';
    }



    public function delete(Host $host)
    {
        $host->delete();
        $this->emit('refreshParent');
    }

    public function save()
    {
        $this->validate();

        foreach ($this->hosts as $host) {
            $host->save();
        }
    }

    public function render()
    {
        if ($this->sortDirection === 'asc') {
            $hosts = Host::orderBy($this->sortBy)->paginate(5);
        } else {
            $hosts = Host::orderByDesc($this->sortBy)->paginate(5);
        }

        return view('livewire.config.hosts',
            ['hosts' => $hosts]
        );
    }


}

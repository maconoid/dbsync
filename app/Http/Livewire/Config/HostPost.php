<?php

namespace App\Http\Livewire\Config;

use App\Http\Traits\WithInlineValidation;
use App\Models\Host;
use Livewire\Component;

class HostPost extends Component
{
    use WithInlineValidation;

    public    $hostname;
    public    $can_ssh;
    protected $rules     = [
        'hostname' => 'required|string|min:6|max:64',
        'can_ssh'  => 'required|string',
    ];
    protected $listeners = [
        'hostsOpenModal'  => 'openModal',
        'hostsCloseModal' => 'closeModal',
    ];

    public function mount()
    {
        $this->can_ssh = 'True';
    }

    public function render()
    {
        return view('livewire.config.host-post');
    }


    public function save()
    {
        $this->validate();

        $host = new Host();
        $host->fill(
            [
                'hostname' => $this->hostname,
                'can_ssh'  => $this->can_ssh,
            ]
        );

        $host->save();

        $this->emit('refreshParent');
        $this->cleanVars();
    }

    private function cleanVars(): void
    {
        $this->hostname = null;
        $this->can_ssh  = 'True';
    }
}

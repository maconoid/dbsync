<?php

namespace App\Http\Livewire\Config;

use App\Http\Traits\WithSorting;
use App\Models\Sync;
use Livewire\Component;

class Syncs extends Component
{
    use WithSorting;

    protected $listeners = [
        'refreshParent' => '$refresh',
    ];

    public function mount()
    {
        $this->sortBy = 'id';
    }

    public function render()
    {

        switch ($this->sortBy) {

            default:
                if ($this->sortDirection === 'asc') {
                    $model = Sync::orderBy($this->sortBy);
                } else {
                    $model = Sync::orderByDesc($this->sortBy);
                }
                break;
        }


        $syncs= $model->paginate(5);
        return view('livewire.config.syncs',
            ['syncs' => $syncs]);
    }

    public function save()
    {
        $this->validate();

        foreach ($this->syncs as $sync) {
            $sync->save();
        }
    }

    public function delete(Sync $sync)
    {
        $sync->delete();
        $this->emit('refreshParent');
    }
}

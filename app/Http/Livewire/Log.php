<?php

namespace App\Http\Livewire;

use App\Http\Traits\WithSorting;
use App\Models\SyncStatus;
use Livewire\Component;

class Log extends Component
{
    use WithSorting;

    public function mount()
    {
        $this->sortBy        = 'created_at';
        $this->sortDirection = 'desc';
    }

    public function render()
    {
        if ($this->sortDirection === 'asc') {
            $model = SyncStatus::orderBy($this->sortBy);
        } else {
            $model = SyncStatus::orderByDesc($this->sortBy);
        }

        $logs = $model->paginate($this->linesPerPage);

        return view('livewire.log', ['logs' => $logs]);
    }
}

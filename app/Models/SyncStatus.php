<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SyncStatus extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $casts = [
        'statusTime' => 'datetime',
    ];

    public function sync()
    {
        return $this->belongsTo(Sync::class, 'sync_id');
    }

    public function action()
    {
        return $this->belongsTo(SyncAction::class, 'action_id');
    }
}

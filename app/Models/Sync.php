<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sync extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'syncname',
        'srchost',
        'srcdir',
        'dsthost',
        'dstdir',
        'dbtype',
    ];

    public function sourceHost()
    {
        return $this->belongsTo(Host::class, 'srchost');
    }

    public function destinationHost()
    {
        return $this->belongsTo(Host::class, 'dsthost');
    }
}

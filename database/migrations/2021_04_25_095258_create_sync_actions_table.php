<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSyncActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sync_actions', function (Blueprint $table) {
            $table->id();
            $table->enum('name', ['TruncLogs', 'SrcCopy', 'DstCopy', 'ApplyBinlog', 'FullDump', 'FullImport'])->nullable();
            $table->foreignId('sync_id')->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sync_actions');
    }
}

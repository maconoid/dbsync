<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSyncStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sync_statuses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sync_id');
            $table->foreign('sync_id')->references('id')->on('syncs')->cascadeOnUpdate()->cascadeOnDelete();
            $table->unsignedBigInteger('action_id');
            $table->foreign('action_id')->references('id')->on('sync_actions')->cascadeOnUpdate()->cascadeOnDelete();
            $table->enum('status', ['SUCCESS', 'ERROR', 'PROGRESS'])->nullable();
            $table->dateTime('statusTime');
            $table->longText('errormessage')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sync_statuses');
    }
}

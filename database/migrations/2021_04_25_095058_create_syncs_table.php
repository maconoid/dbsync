<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSyncsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('syncs', function (Blueprint $table) {
            $table->id();
            $table->string('syncname');
            $table->unsignedBigInteger('srchost');
            $table->foreign('srchost')->references('id')->on('hosts')->cascadeOnUpdate()->cascadeOnDelete();;
            $table->string('srcdir');
            $table->unsignedBigInteger('dsthost');
            $table->foreign('dsthost')->references('id')->on('hosts')->cascadeOnUpdate()->cascadeOnDelete();;
            $table->string('dstdir');
            $table->string('dbtype');
            $table->string('mostrecent');
            $table->timestamp('last_action');
            $table->integer('last_status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('syncs');
    }
}
